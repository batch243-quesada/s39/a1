const User = require('../models/Users');
const auth = require('../auth')

const bcrypt = require('bcrypt');

// Check if email aready exists
// 1. Use mongoose "find" method to find duplicate emails
// 2. Use the then "method" to send a response to the frontend app based on the result of the find method 
const checkEmailExists = (request, response, next) =>{
	
	// The result is sent back to the front end via the "then" method
	return User.find({email: request.body.email}).then(result => {
		let message;
		// The "find" method returns an array of record of matching documents

		if(result.length > 0) {
			message = `The ${request.body.email} is already taken. Please use another email.`
			return response.send(message);
		} else {
			next();
		}
	})
}

const registerUser = (request, response) => {

	// creates a variable named "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the information from request body to provide the necessary information
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,

		// salt - salt rounds that bcrypt algorithm will run to encrypt the password
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	// Saves the created object to our database
	return newUser
	.save()
	.then(user => {
		console.log(user);
		response.send(`Thank you, ${user.firstName}. You are now registered.`);
	}).catch(error => {
		console.log(error);
		response.send(`Sorry, ${newUser.firstName}, there was an error during the registration. Please try again.`)
	})
}

// User Authentication
// 1. Check db if the user email exists
// 2. Compare the password provided in the login form with the password stored in the db
const loginUser = (request, response) => {
	return User.findOne({email: request.body.email})
	.then(result => {
		console.log(result);
		if(result === null) {
			response.send(`This email doesn't exist.`)
		} else {
			// compareSync() method is used to compare a non encrypted password from the login form to the encrypted password retrieved
			// it will return true or false value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({accessToken: auth.createAccessToken(result)});
			} else {
				return response.send(`Incorrect password, please try again.`);
			}
		}
	})
}

const getProfile = (request, response) => {
	return User.findOne({_id: request.body.id})
	.then(result => {
		if (result === null) {
			return response.send(`ObjectId is not found!`)
		} else {
			result.password = "*********";
			return response.send(result);
		}
	})
}

const profileDetails = (request, response) => {
	// user will be object that contains the id and email of the user currently logged in
	const userData = auth.decode(request.headers.authorization);

	return User.findById(userData.id)
	.then(result => {
		result.password = "Confidential";
		return response.send(result);
	}).catch (err => {
		return response.send(err);
	})
}

// Sir Chris solution
// module.exports.getProfile = (request, response) =>{

// 	return User.findById(request.body.id).then(result => {
// 		result.password = "******";
// 		console.log(result);
// 		return response.send(result);
// 	}).catch(error => {
// 		console.log(error);
// 		return response.send(error);
// 	})
// }

module.exports = {
	checkEmailExists,
	registerUser,
	loginUser,
	getProfile,
	profileDetails
}