const Course = require('../models/Courses');
const User = require('../models/Users');
const	auth = require('../auth');

// 1. Create a new Course object using mongoose model and information from the request
// 2. Save the new course to the database
const addCourse = (request, response) => {
	let newCourse = new Course({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots
	})

	const userData = auth.decode(request.headers.authorization);
	if(userData.isAdmin !== false) {
		return newCourse
		.save()
		.then(result => {
			console.log(result)
			response.send(true)
		}).catch(error => {
			console.log(error);
			response.send(false);
		})
	} else {
		return response.send('Access denied.');
	}
	
}

module.exports = {
	addCourse
}