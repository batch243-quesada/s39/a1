// We require the JWT module and then contain it in jwt variable
const jwt = require('jsonwebtoken');

// used in algorithm for encrypting our data which makes it difficult to decode the information without defined secret key
const secret = "CourseBookingAPI"

/// JSON Web token

// Token creation
// Analogy
// Pack the gift provided with a lock, which can only be opened using the secret code as the key.

// the parameter user is from userController.loginUser
module.exports.createAccessToken = (user) => {
	// payload of the JWT
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's sign method.
	// Syntax
	// jwt.sign(payload, secretOrPrivatekey, [callbackfunction])
	return jwt.sign(data, secret, {})
}

/// Token Verification
// Analogy
// receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if (token !== undefined) {
		// Validate the "token" using verify() method to decrypt the token using the secret code.header
		// Syntax
		// jwt.verify(token, secret, [callback function])
		token = token.slice(7, token.length);
		console.log(token);

		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return response.send('Invalid Token');
			} else {
				next();
			}
		})
	} else {
		return response.send('Authentication failed! No token provided.');
	}	
}

//// Token decryption
// Analogy
// Opening gift or unwrapping presents
module.exports.decode = (token) => {
	if(token === undefined) {
		return null;
	} else {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return null;
			} else {
				// decode method isused to obtain the information from the JWT
				// Syntax: jwt.decode(token, [option])
				// Returns an object with the access to the payload property
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
}