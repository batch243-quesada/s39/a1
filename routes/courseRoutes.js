const express = require('express');
const auth = require('../auth');

const router = express.Router();

const courseControllers = require('../controllers/courseControllers');


router.post("/", auth.verify, courseControllers.addCourse);

module.exports = router;